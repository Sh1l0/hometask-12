'use strict';
/*
  Вам необходимо добавить обработчики событий в соотвествии с заданием.
  Вам уже даны готовые стили и разметка. Менять их не нужно,
  за исключением навешивания обработчиков и добавления data - аттрибутов

*/
/*Задание 1
  Необходимо сделать выпадающее меню.
  Меню должно открываться при клике на кнопку,
  закрываться при клике на кнопку при клике по пункту меню
*/
document.getElementById('menu').addEventListener('click', () => {
  document.querySelector('.menu').classList.remove('menu-opened');
});
document.getElementById('menuBtn').addEventListener('click', () => {
  document.querySelector('.menu').classList.toggle('menu-opened');
});
/**
 * Задание 2
 * Необходимо реализовать перемещение блока по клику на поле.
 * Блок должен перемещаться в место клика.
 * При выполнении задания вам может помочь свойство element.getBoundingClientRect()
 */
const obj = document.getElementById('movedBlock');
const field = document.getElementById('field');
document.getElementById('field').addEventListener('click', event => {
  let x = event.clientX - field.getBoundingClientRect().x;
  let y = event.clientY - field.getBoundingClientRect().y;
  obj.style.left = `${x}px`;
  obj.style.top = `${y}px`;
});
console.log(obj);
/**
 * Задание 3
 * Необходимо реализовать скрытие сообщения при клике на крестик при помощи делегирования
 */
const messanger = document.getElementById('messager');
messager.addEventListener('click', e => {
  if(event.target.classList.contains('remove')) {
      event.target.parentNode.style.display = "none";
  }
});
/**
 * Задание 4
 * Необходимо реализовать вывод сообщения при клике на ссылку.
 * В сообщении спрашивать, что пользователь точно хочет перейти по указанной ссылке.
 * В зависимости от ответа редиректить или не редиректить пользователя
 */
 const links = document.querySelector('#links');
 links.addEventListener('click', event => {
   if(event.target.hasAttribute('href')) {
     event.preventDefault();
     if(confirm("Вы уверены, что хотите перейти по ссылке?")) {
       window.location.href = event.target.getAttribute('href');
     }
   }
 });
  /**
  * Задание 5
  * Необходимо сделать так, чтобы значение заголовка изменялось в соотвествии с измением
  * значения в input
  */
  const input = document.getElementById('fieldHeader');
  const header = document.getElementById('taskHeader');
  input.addEventListener('keyup', event => {
    header.innerHTML = event.target.value;
    if(header.innerHTML === "") {
      header.innerHTML = "Заголовок";
        }
  });
